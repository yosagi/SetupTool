#
# _10_Local_hdfview.ps1
#

$portablePackageDefs=@(@{
 name="hdfview";
 archive="https://support.hdfgroup.org/ftp/HDF5/hdf-java/current/bin/HDFView-2.14-win_64. zip";
 archive_referer="https://support.hdfgroup.org/products/java/release/download.html";
 check=@("MD5","76989C99A944E025D64B4959BD8B32BA");
# extract=@("hdfview",$(Join-Path $STAppPrefix hdfview));
 })

# zip に msi がある。chocolatey package に仕立てた方がよさそう
# install-targets $portablePackageDefs
