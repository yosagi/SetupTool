#
# _10_Local_vim_plugins.ps1
#

$portablePackageDefs=@(@{
 name="vim";
    archive = "https://github.com/koron/vim-kaoriya/releases/download/v8.1.0005-20180520/vim81-kaoriya-win64-8.1.0005-20180520.zip"
    check   = @("MD5", "1BDAF842DFCC2A40A866854B72C8D0A6");
    extract=@("vim81-kaoriya-win64",$(Join-Path $STAppPrefix vim-8.1.0));
 }
)

install-targets $portablePackageDefs
