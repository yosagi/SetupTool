﻿#
# _01_Base_CtrlRemap.ps1
#

  #      参考: http://www.jaist.ac.jp/~fujieda/scancode.html
  # 主要なコード(上記Webより)
  #  半角/全角	0x29
  #  英数		0x3a
  #  左Ctrl		0x1d
  #  右Ctrl		0xe01d
  #  左Alt		0x38
  #  右Alt		0xe038
  #  無変換		0x7b
  #  変換		0x79
  #  ひらがな	0x70
  #  左Win		0xe05b
  #  右Win		0xe05c
  #  App		0xe05d

# スキャンコードを差し替えるレジストリエントリ
[Byte[]]$RemappingCodeCtrl=@(
	00,00,00,00,         # Header (0x00000000)
	00,00,00,00,         # Header (0x00000000)
  02,00,00,00,         # エントリ数 (0x00000002)              ## ヌルターミネータを含む
	0x1d,00,0x3a,00,     # 英数    (0x003a) → 左 Ctrl (0x001d)
	00,00,00,00          # Null Terminator (0x00000000)
)

[Byte[]]$RemappingCodeRFMac = @(
  00, 00, 00, 00,         # Header (0x00000000)
  00, 00, 00, 00,         # Header (0x00000000)
  0x09, 00, 00, 00,       # エントリ数 (0x00000009)  ## ヌルターミネータを含む
  0x1d, 00, 0x3a, 00,     # 英数    (0x003a) → 左 Ctrl (0x001d)
  0x1d, 0xe0, 0x1d, 00,   # 左 Ctrl (0x001d) → 右 Ctrl (0xe01d)
  # Win <-> Alt
  0x38, 0x00, 0x5b, 0xe0, # 左 Win(0x5b)   →  左Alt (0x38)
  0x38, 0xe0, 0x5c, 0xe0, # 右 Win(0xe05c) →  右Alt (0xe038)
  0x5b, 0xe0, 0x38, 00,   # 左 Alt(0x38)   →  左Win (0x5b)
  0x5c, 0xe0, 0x38, 0xe0, # 右 Alt(0xe038) →  右Win (0xe05c)
  0x38, 0xe0, 0x5d, 0xe0, # 右 App(0xe05d) →  右Alt (0xe038)
  # Numlock -> Insert
  0x52, 0xe0, 0x45, 0x00  # Clear -> Ins
  00, 00, 00, 00          # Null Terminator (0x00000000)
)

$RemappingCode=$RemappingCodeCtrl

$Key="HKLM:\SYSTEM\CurrentControlSet\Control\Keyboard Layout"
$CurrentMap=(Get-ItemProperty -Path $Key)."Scancode Map"
# remap
if(($CurrentMap.length -eq 0) -or ((Compare-Object $RemappingCodeCtrl $CurrentMap).length -ne 0)){
	Write-Host "Installing a new key-remapping registry entry"
	Set-ItemProperty -path $key -name "Scancode Map" -Value $RemappingCode
}
