#
# _00_Base_Scoop.ps1
#

#$spath=Split-Path -parent $MyInvocation.MyCommand.Path

# Package Response file template
$pkgheader=@"
<?xml version="1.0" encoding="utf-8"?>
    <packages>
"@
$pkgfooter=@"
    </packages>
"@

Get-Command scoop -ErrorAction Ignore
if (-not $(get-command scoop ErrorAction Ignore)){
  Write-Host "Scoop is not installed. Installing scoop for current user."
  iex (new-object net.webclient).downloadstring('https://get.scoop.sh')
}
# PATH 更新
Update-Environment
# install packages
## multiple connection downloader
scoop install aria2
# add extras bucket
scoop bucket add extras
