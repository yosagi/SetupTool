﻿#
# _01_Base_CMake.ps1
#

# パッケージでの対応が不完全な Shim executable の生成

Generate-ChocoShim -target "cmake\bin\cmake-gui.exe" -parents @("${env:ProgramFiles(x86)}","${env:ProgramFiles}")
Generate-ChocoShim -target "cmake\bin\cmake.exe" -parents @("${env:ProgramFiles(x86)}","${env:ProgramFiles}")
