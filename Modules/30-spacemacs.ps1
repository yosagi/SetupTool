#
# 30-spacemacs.ps1
#    spacemacs and some configurations


$spacemacsDir=Join-Path $env:USERPROFILE ".spacemacs.d"
$dotEmacsDir=Join-Path $env:USERPROFILE ".emacs.d"
if (Test-Path $dotEmacsDir ){
	Write-Host "[$dotEmacsDir] exists. Make a backup of [.emacs.d] and try again."
}else{
	git	clone https://github.com/syl20bnr/spacemacs.git	$dotEmacsDir
}
if (Test-Path $spacemacsDir){
	Write-Host "[$spacemacsDir] exists. "
}else{
	Ensure-dir $spacemacsDir
	Ensure-dir $(Join-Path $spacemacsDir "Layers")
	git clone https://gitlab.com/yosagi/spacemacs-setting.git $(Join-Path $spacemacsDir "Layers\yos")
    if(-not(Test-Path $spacemacsDir+"\init.el")	){
		New-Item -ItemType SymbolicLink -Path $(Join-Path $spacemacsDir "init.el") -Value $(Join-Path $spacemacsDir "Layers\yos\init.el.win")
	}
}
