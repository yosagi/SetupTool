#
# _10_Local_emacs.ps1
#

$portablePackageDefs=@(@{
 name="emacs";
# archive="https://github.com/chuntaro/NTEmacs64/raw/master/emacs-24.5-IME-patched.zip";
# check=@("MD5","95DA59C402F5A277A0E8A1D982DCEB5A");
# extract=@("emacs-24.5-IME-patched",$(Join-Path $STAppPrefix emacs-24.5));
# archive="https://github.com/chuntaro/NTEmacs64/raw/master/emacs-25.1-IME-patched.zip";
# check=@("MD5","C4890A75F9E48A26850259B649383024");
# extract=@("emacs-25.1",$(Join-Path $STAppPrefix emacs-25.1));
# archive="https://github.com/chuntaro/NTEmacs64/raw/master/emacs-25.2-IME-patched.zip";
# check=@("MD5","D26AD6A348A498A08FCADAD996C6AF2F");
# extract=@("emacs-25.2",$(Join-Path $STAppPrefix emacs-25.2));
 archive="https://github.com/mhatta/emacs-26-x86_64-win-ime/raw/master/emacs-26.1-x86_64-win-ime-20180619.zip"
 check=@("MD5","0FF560F6C6C4FF230199C69BA8DC6AC4");
 extract=@("emacs-26.1",$(Join-Path $STAppPrefix emacs-26.1));
 }
)

install-targets $portablePackageDefs
