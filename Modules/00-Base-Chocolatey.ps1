#
# _00_Base_Chocolatey.ps1
#

#$spath=Split-Path -parent $MyInvocation.MyCommand.Path

# Package Response file template
$pkgheader=@"
<?xml version="1.0" encoding="utf-8"?>
    <packages>
"@
$pkgfooter=@"
    </packages>
"@

# PackageManagement 環境設定
# install chocolatey
if(-not	$(get-command choco	-ErrorAction Ignore))
{
    Write-Host "Installing Chocolatey"
    iex	((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
}
# install packages

Get-ChildItem $(Join-Path $spath "ChocoPackages*.list") | %{
	$pkglist+=Get-Content $_ 
}
if($pkglist.Length -eq 0){
	Get-ChildItem $(Join-Path $spath "ChocoPackages-DefaultList.list") | %{
		$pkglist+=Get-Content $_
	}
}
# Setup response file for chocolatey
$pkgconfig=Join-Path $spath "_ChocoPackages-toInstall.config"
# パッケージ名の列挙からchocolateyに渡すxmlフォーマットを生成
$pkglistraw=$pkglist -split "\n" | %{($_ -replace "#.*" ) -replace "\s"}
$pkgheader | Set-Content -path $pkgconfig
$pkglistraw |where {$_ -match "[0-9a-z]"} | %{ '<package id="' + ($_ -replace '`n') + '" />' | Add-Content -Path $pkgconfig}
$pkgfooter | Add-Content -Path $pkgconfig

chocolatey install -y --allow-empty-checksums $pkgconfig
Write-Host "[RUN] chocolatey install -y --allow-empty-checksums $pkgconfig"
rm $pkgconfig

Append-Path "C:\ProgramData\Chocolatey\Tools"

# PATH 更新
Update-Environment
