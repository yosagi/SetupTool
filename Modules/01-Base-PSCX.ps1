#
# _01_Base_PSCX.ps1
#

if($false){
if(-not($env:PSModulePath -match "pscx"))
{
	if(Test-Path $(Join-Path ${Env:ProgramFiles(x86)} "\Powershell Community Extensions" )){
		$pscxpath=$(Join-Path $(Join-Path ${Env:ProgramFiles(x86)} "\Powershell Community Extensions" ) PSCX3)
	}elseif(Test-Path $(Join-Path ${Env:ProgramFiles} "\Powershell Community Extensions" )){
		$pscxpath=$(Join-Path $(Join-Path ${Env:ProgramFiles} "\Powershell Community Extensions" ) PSCX3)
	}else{
		Throw "Could not find Power shell community extention in standard places."
	}
    Write-Host "Adding pscx path[$pscxpath] to PSModulePath"
	Add-EnvVar -Var "PSModulePath" -Entry "$pscxpath" -Machine
}
}