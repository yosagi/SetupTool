#
# _01_Base_Anaconda.ps1
#

$ChocolateyToolsLocation=$env:ChocolateyToolsLocation

if ( $ChocolateyToolsLocation -eq $null ){
	$ChocolateyToolsLocation="C:\tools"
}
if ( -not (Test-Path $ChocolateyToolsLocation )){
	Write-Error	"Could not find	ChocolateyToolsLocation	path"
	return
}else{
    if (Test-ChocoPkg Anaconda2	){
        $AnacondaPath=$(Join-Path $ChocolateyToolsLocation "Anaconda2")
        $AnacondaScriptsPath=$(Join-Path $AnacondaPath "Scripts")
        if (Test-Path $AnacondaPath){
            Write-Host "found anaconda2 chocolatey package"
            Append-Path	$AnacondaPath
        }
        if (Test-Path $AnacondaScriptsPath){
            Append-Path	$AnacondaScriptsPath
        }
    }else{
        Write-Host "Anaconda2 chocolatey package is	not	installed."
    }
    if (Test-ChocoPkg Anaconda3	){
        $AnacondaPath=$(Join-Path $ChocolateyToolsLocation "Anaconda3")
        $AnacondaScriptsPath=$(Join-Path $AnacondaPath "Scripts")
        if (Test-Path $AnacondaPath){
            Write-Host "found anaconda3 chocolatey package"
            Append-Path	$AnacondaPath
        }
        if (Test-Path $AnacondaScriptsPath){
            Append-Path	$AnacondaScriptsPath
        }
    }else{
        Write-Host "Anaconda3 chocolatey package is not installed."
    }
}

