#
# _20_User_Powerline.ps1
#
# https://docs.microsoft.com/ja-jp/windows/terminal/tutorials/powerline-setup

Install-Module posh-git -Scope CurrentUser
Install-Module oh-my-posh -Scope CurrentUser

if( -not (Test-Path $profile) -or ( $(Select-String "Set-Theme" $profile).length -ne 1)){
  Write-Output "Set-Theme Paradox" | Add-Content $profile -Encoding utf8
}
