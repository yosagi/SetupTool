﻿#
# 20-User-Code.ps1
#

if(-not $(Get-Command code)){
    Write-Host "VSCode is not installed"
    return
}

# インストールする拡張リスト

if($codeExtentions -eq $null) {
$codeExtentions=@(
    "AlanWalk.markdown-toc",
    "DavidAnson.vscode-markdownlint",
    "geddski.macros",
    "hnw.vscode-auto-open-markdown-preview",
    "IBM.output-colorizer",
    "lfs.vscode-emacs-friendly",
    "mitaki28.vscode-clang",
    "ms-vscode.cpptools",
    "selbh.keyboard-scroll",
    "twxs.cmake",
    "vector-of-bool.cmake-tools",
    "xaver.clang-format"
)
}

# インストールするキーバインド設定の内容

if($codeKeybindings -eq $null){
$codeKeybindings=@"
// 既定値を上書きするには、このファイル内にキー バインドを挿入します
[
// goto line
{
  "key": "alt+g",
  "command": "workbench.action.gotoLine"
},
// emacs like open line (settings.jsonにmacrosで実装)
{
  "key": "ctrl+o",
  "command": "macros.openLine",
  "when": "editorTextFocus"
},
{
  "key": "ctrl+;",
  "command": "editor.action.triggerSuggest",
  "when": "editorTextFocus"
},
{
  "key": "ctrl+;",
  "command": "toggleSuggestionDetails",
  "when": "editorTextFocus && suggestWidgetVisible"
},
{
  "key": "ctrl+shift+;",
  "command": "editor.action.triggerParameterHints",
  "when": "editorTextFocus"
}
]
"@
}

# インストールする設定ファイルの内容

if($codeSettings -eq $null){
$codeSettings=@"
{
    "[cpp]": {
        "editor.quickSuggestions": false
    },
    "[c]": {
        "editor.quickSuggestions": false
    },
    "clang.cxxflags": [
        "--std=c++11"
    ],
    "editor.renderWhitespace": "boundary",
    "editor.tabSize": 2,
    "editor.formatOnPaste": true,
    "editor.fontLigatures": true,
    "editor.minimap.enabled": true,
    "todohighlight.include": "{**/*.c,**/*.cc,**/*.cpp,**/*.h,**/*.hh,**/*.hpp,**/*.js,**/*.jsx,**/*.ts,**/*.html,**/*.php,**/*.css,**/*.scss}",
    "workbench.colorTheme": "Quiet Light",
// macros extention を使った emacs open line 的挙動
    "macros": {
        "openLine": [
            {"command": "type",
            "args": {
                "text": "\n"
            }},
            "cursorUp",
            "cursorEnd"
        ]
    }
}
"@
}

$codeexe=$(Get-Command "code").path

foreach($ext in $codeExtentions){
    Write-Host "$codeexe --install-extension $ext"
    & "$codeexe" --install-extension "$ext"
}

$configbase=Join-path $env:USERPROFILE "AppData/Roaming/Code/User"
$keybindingspath= "keybindings.json"
$settingspath=    "settings.json"

if(Test-Path $(Join-Path $configbase $keybindingspath)){
    Write-Host "'keybindings.json' が存在します。'keybindings-new.json' としてインストールします。"
    $keybindingspath= "keybindings-new.json"
    if(Test-Path $(Join-Path $configbase $keybindingspath)){
        Remove-Item $(Join-Path $configbase $keybindingspath)
    }
}

if(Test-Path $(Join-Path $configbase $settingspath)){
    Write-Host "'settings.json' が存在します。'settings-new.json' としてインストールします。"
    $settingspath= "settings-new.json"
    if(Test-Path $(Join-Path $configbase $settingspath)){
        Remove-Item $(Join-Path $configbase $settingspath)
    }
}

New-Item -Path $configbase -Name $keybindingspath -ItemType File -Value $codeKeybindings -ErrorAction SilentlyContinue | Out-Null
New-Item -Path $configbase -Name $settingspath -ItemType File -Value $codeSettings -ErrorAction SilentlyContinue | Out-Null
