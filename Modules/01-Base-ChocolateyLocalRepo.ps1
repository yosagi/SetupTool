﻿#
# _01_Base_ChocolateyLocalRepo.ps1
#

$pkgheader=@"
<?xml version="1.0" encoding="utf-8"?>
    <packages>
"@
$pkgfooter=@"
    </packages>
"@

$DefaultChocolateyPackageList=@"
ttpage
"@

if($STLocalPkgSource.length -eq 0){
	$STLocalPkgSource="..\..\choco"
}
$pkglist=""

if(-not $(Test-Path $STLocalPkgSource))
{
	Write-Error "Local Package Source[$STLocalPkgSource] does not exist"
	return
}

# install chocolatey
if(-not $(get-command choco -ErrorAction Ignore))
{
	iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
}

# install packages
Get-ChildItem $(Join-Path $spath "LocalChocoPackages*.list") | %{
	$pkglist+=Get-Content $_ 
}
if($pkglist.Length -eq 0){
	$DefaultChocolateyPackageList | Set-Content -Path $(Join-Path $spath "LocalChocoPackages-DefaultList.list")
	$pkglist=$DefaultChocolateyPackageList
}
# Setup response file for chocolatey
$pkgconfig=Join-Path $spath "_LocalChocoPackages-toInstall.config"
# コメント、空白、改行を削除し、xmlファイルを出力
$pkglistraw=$pkglist -split "\n" | %{($_ -replace "#.*" ) -replace "\s"}

$pkgheader | Set-Content -path $pkgconfig
$pkglistraw |where {$_ -match "[0-9a-z]"} | %{ '<package id="' + ($_ -replace '`n') + '" />' | Add-Content -Path $pkgconfig}
$pkgfooter | Add-Content -Path $pkgconfig

#echo $((($packages -split "\n" | % {$_ -replace "#.*"} )  -join " " ) -replace " +"," ")


Write-Host "[RUN] chocolatey install -y --allow-empty-checksums $pkgconfig -s $(Join-Path $spath ..\..\choco) -f"
chocolatey install -y --allow-empty-checksums $pkgconfig -s $(Join-Path $spath ..\..\choco) -f
rm $pkgconfig