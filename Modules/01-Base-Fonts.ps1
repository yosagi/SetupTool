#
# _00_Local_Fonts.ps1
#

$fonttargets = @(
@{
 name="migmix-1m";
 archive="http://osdn.jp/frs/redir.php?m=iij&f=%2Fmix-mplus-ipa%2F63544%2Fmigmix-1m-20150712.zip";
 check=@("MD5","3de659097dc4d6b156040c5c25dc4cf9")
 installfonts="migmix*\*.ttf";
 },
@{
 name="migmix-1p";
 archive="http://osdn.jp/frs/redir.php?m=iij&f=%2Fmix-mplus-ipa%2F63544%2Fmigmix-1p-20150712.zip";
 check=@("MD5","6e68c94b3a9cd8e549d32a5edde8c7a5")
 installfonts="migmix*\*.ttf";
 },
@{
 name="migu-1m";
 archive="http://osdn.jp/frs/redir.php?m=iij&f=%2Fmix-mplus-ipa%2F63545%2Fmigu-1m-20150712.zip";
 check=@("MD5","80046949f85da8177d6f1bb319f744f8")
 installfonts="migu*\*.ttf";
 },
@{
 name="migu-2m";
 archive="http://osdn.jp/frs/redir.php?m=iij&f=%2Fmix-mplus-ipa%2F63545%2Fmigu-2m-20150712.zip";
 check=@("MD5","6dac0f64af30cfb006e27d071cfb9f97")
 installfonts="migu*\*.ttf";
 }
@{
 name="Adobe Source Han Code JP";
 archive="https://github.com/adobe-fonts/source-han-code-jp/archive/2.000R.zip";
 check=@("MD5","08C4378A466A00B13365B563C44610A7");
 installfonts="source-han-code-jp-2.000R\OTF\*.otf"
}
@{
 name="Ricty Diminished";
 archive="https://github.com/watiko/fonts-rictydiminished/archive/master.zip";
 check=@("MD5","807113BCA480AADF53525A60C1AE6216");
 installfonts="fonts-rictydiminished-master\*.ttf"
}
@{
 name="Myrica";
 archive="https://github.com/tomokuni/Myrica/raw/master/product/Myrica.zip";
 check=@("MD5","61E2A3A2F43910082DD7C2DC53A11ADD"); # 2.012.20180119
 #check=@("MD5", "D85AB451D7C90CEC714F20FF417F145B");
 installfonts="Myrica.TTC";
}
@{
 name="MyricaM";
 archive="https://github.com/tomokuni/Myrica/raw/master/product/MyricaM.zip";
 check=@("MD5","4BF361B455B4B9AB6AB971BC5392E0D0");  # 2.012.20180119
 #check=@("MD5","2CDCCB8361BE4982257CC2FAB4790290");
 installfonts="MyricaM.TTC";
}
@{
 name="Cica";
 archive="https://github.com/miiton/Cica/releases/download/v5.0.1/Cica_v5.0.1_with_emoji.zip";
 check=@("MD5","BF868D4B865959B2E40AC65E34CBE92C");
 installfonts="Cica*.ttf";
}
)
Write-Host "Installing migmix, migu fonts"
install-fontTargets $fonttargets
