#
# _10_Local_gitbug.ps1
#

$portablePackageDefs=@( @{
 name="gitbug";
    #archive = "https://github.com/MichaelMure/git-bug/releases/download/0.5.0/git-bug_windows_amd64.exe";
    #check   = @("MD5", "E5D3576C8222C2BF4B5738B3246033D4");
    archive = "https://github.com/MichaelMure/git-bug/releases/download/0.7.1/git-bug_windows_amd64.exe";
    check = @("MD5", "9EF4716E251B29399DE63DF6BED86E64");
    singleExe = "git-bug.exe"
    enable    = $true
 }
)

install-targets $portablePackageDefs
