#
# Setup_Module.ps1
#
[CmdletBinding(SupportsShouldProcess=$True)]
param(
   [Parameter(Mandatory=$false, Position=0)]            
    [string]$filter,
    [Switch]$dryrun,
    [Switch]$nondefault,
    [Switch]$all
#    [Sqitch]$Confirm
)
if($filter.length -eq 0){
    $all=$true
}

$cmdline=$myinvocation.line
$arguments=$cmdline.substring($cmdline.indexof(" "))
$spath=Split-Path -parent $MyInvocation.MyCommand.Path

# install SetuptoolFunctions module
$moduledir="$env:USERPROFILE\Documents\WindowsPowershell\Modules"
$moduleToInstall=@("SetuptoolFunctions")
foreach($mod in $moduleToInstall) {
    $src=$(Join-Path $(Join-Path $spath $mod) $mod".psm1")
    $mtime=$(ls $src).LastWriteTime
    $destdir=$(Join-Path $moduledir $mod)
    $dest=$(Join-Path $destdir $mod".psm1")
    $dmtime=$(ls $dest).LastWriteTime
    if( -not (Test-Path $destdir)){
        New-Item -itemType "directory" $destdir
    }
    if( -not (Test-Path $dest)){
        Copy-Item $src $dest
        import-module -Force $src -ErrorAction Ignore
    }
    if((Test-Path $dest -OlderThan $mtime)){
        Write-Host "Dest $dest is older than $mtime. Updating it."
        Copy-Item $src $dest
        import-module -Force $src -ErrorAction Ignore
    }
}


if(-not(Test-Path $moduledir)){
    new-item -ItemType "directory" -Name $moduledir
}

. (Join-Path $spath "Setup-Config.ps1")

if(-not($?))
{
    Write-Host "Could not import-module SetuptoolFunctions"

}

$cmd=$(Join-Path $spath "Setup-Module.ps1")
$cmd+=$arguments

if(-not (Test-Elevated ) -and -not $dryrun){
    if($PSCmdlet.ShouldProcess("Setup-Module.ps1","Execute in elevated shell")){
        Enter-ElevatedPS "$cmd"
    }
}

Write-Host $filter
Get-ChildItem $(Join-Path $spath "Modules\[0-9][0-9]*.ps1") | Where-Object {$_.Name -match "[0-9][0-9]X?.*ps1" }| %{
    $mod=Split-Path -leaf $_
    $skip=$mod -match "^[0-9][0-9]X-"
    $filtermatch = $mod.ToLower().Contains($filter.ToLower()) -and $filter.Length -gt 0
    if (($all -or $filtermatch ) -and ( $nondefault -or -not $skip )) {
        Write-Host -ForegroundColor Green "Running $mod"
        if(-not $dryrun -and $PSCmdlet.ShouldProcess($mod, "Run script")){
            . $_
        }
    }else{
        Write-Host -ForegroundColor Yellow "Skipping $mod"
    }

}
Write-Host -ForegroundColor Green "Done."
pause
exit
