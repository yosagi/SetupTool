#
# Setup_psfuncs.ps1
#
# 雑多な関数集

# 管理者権限を持っているかチェック
#function Test-Elevated

# $newpath をユーザ環境変数のPATHに追加する。すでに入っていたら何もしない
#function Append-Path{

# タスクスケジューラにタスクを登録する  (動作しない環境がある)
#function Register-StartupTask{

# zip/7z アーカイブを展開する
#function Expand-ArchiveTo{

# 実行ファイルの互換性フラグを設定する
# function Set-BinaryCompatibilityFlags

# Web からファイルをダウンロード
# function Get-WebFile

# ZIP を展開
# function Expand-ZIPFile($file, $destination)

# Webからファイルをダウンロード(キャッシュがあればダウンロードしない)
# function Fetch-File

# Webからファイルをダウンロード(キャッシュがあればダウンロードしない) 改良版
# function Fetch-File2


# ディレクトリをチェックして存在しなければ作る
# function Check-destdir

# プロセスを起動
# https://github.com/guitarrapc/PowerShellUtil/blob/master/Invoke-Process/Invoke-Process.ps1
# Invoke-Process

# パッチを適用
# Apply-Patch ($patch, $WorkingDirectory, $p, $sysinfo)
# 以下の順で探して見つかった patch.exe を使う
#  $sysinfo["patch.exe"]
#  $sysinfo["gnupackbin"]\patch.exe
#  $sysinfo["gitbin"]\patch.exe

# Read-Confirmation
# https://stackoverrun.com/ja/q/11077495

# Path
$ProgramDataPath = [Environment]::GetFolderPath("CommonApplicationData")
$ChocolateyLibPath=$(Join-Path $ProgramDataPath "chocolatey/lib")
$ChocolateyBinPath=$(Join-Path $ProgramDataPath "chocolatey/bin")
$ChocolateyToolsPath=$(Join-Path $ProgramDataPath "chocolatey/tools")

# 確認要求 (https://stackoverrun.com/ja/q/11077495)
function Read-Confirmation { 
	Param(
		[Parameter(Mandatory = $false)] 
		[string]$Prompt, 
		[Parameter(Mandatory = $false)] 
		[string]$Message 
	) 

	$choices = New-Object Collections.ObjectModel.Collection[Management.Automation.Host.ChoiceDescription] 
	$choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&Yes')) 
	$choices.Add((New-Object Management.Automation.Host.ChoiceDescription -ArgumentList '&No')) 

	-not [bool]$Host.UI.PromptForChoice($Message, $Prompt, $choices, 1) 
} 

# 管理者権限を持っているかチェック
function Test-Elevated
{
	# https://blogs.msdn.microsoft.com/virtual_pc_guy/2010/09/23/a-self-elevating-powershell-script/
	# Get the ID and security principal of the current user account

	$myWindowsID=[System.Security.Principal.WindowsIdentity]::GetCurrent()
	$myWindowsPrincipal=new-object System.Security.Principal.WindowsPrincipal($myWindowsID)

	# Get the security principal for the Administrator role
	$adminRole=[System.Security.Principal.WindowsBuiltInRole]::Administrator

	# Check to see if we are currently running "as Administrator"
	if ($myWindowsPrincipal.IsInRole($adminRole))
	{
		return $true
	}
	else
	{
		return $false
	}
}

function Enter-ElevatedPS
{
    param([String] $argstr,
    [Switch] $noexit)
    if(Test-Elevated)
	{
        $Host.UI.RawUI.WindowTitle = $myInvocation.MyCommand.Definition + "(Elevated)"
        $Host.UI.RawUI.BackgroundColor = "DarkBlue"
		return
	}
	else
	{
        Write-Host "Start-Process -wait -Verb runas -Argument $argstr"
        Start-Process powershell -argument $argstr -Wait -Verb runas
        # Exit from the current, unelevated, process
        if(-not $noexit){
          exit
        }
	}
}

#############################################################################

# こんな定義のデータを食わせると適当にダウンロードしてシステムにインストールする
if($false){
@(
 @{  # Portable Application の場合
 name="sample";
 archive="http://...zip";
 check=@("MD5","");                                       #Get-FileHash の -Algorithm に渡すパラメータと、結果のHash値の組
 extract=@("keyhac",$(Join-Path $STAppPrefix keyhac_170));  #アーカイブ内のコピー元フォルダと、インストール先フォルダの組
#enable=$false;                                           # $falseなら処理しない
 },
 @{  # フォントの場合
 name="sample";
 archive="http://...zip";
 check=@("MD5","");           #Get-FileHash の -Algorithm に渡すパラメータと、結果のHash値の組
 installfonts="hoge*.ttf";    # インストールするフォントのアーカイブ内のパス。ワイルドカード使用可。
#enable=$false;               # $falseなら処理しない
 }
)
}

function install-targets{
param([Object[]] $PKGDEFS)
	foreach($item in $PKGDEFS){
		if(-not ($item.enable -eq $false))
		{
			Write-Host "Processing $($item.name)"
			install-singletarget $item
		}
	}
}

function install-fontTargets{
param([Object[]] $PKGDEFS)
	foreach($item in $PKGDEFS){
		if(-not ($item.enable -eq $false))
		{
			Write-Host "Processing $($item.name)"
			install-singletarget $item
		}
	}
}

function install-singletarget{
	param([Object] $Item)
	Write-Host "Downloading $($Item.name)"
    $referer = ""
    if($Item.Contains("archive_referer")){
        $referer=$Item.archive_referer
    }
    $FILE=Fetch-File2 -URL $Item.archive -CHECK $Item.check -referer $referer -storedir $STDownloads
#  Font のインストール   http://stackoverflow.com/questions/16023238/installing-system-font-with-powershell
	if($Item.installfonts -ne $null){
		$FontDirID=0x14
		$objShell=New-Object -ComObject Shell.Application
		$objFonter = $objShell.Namespace($FontDirID)

		$tmpdir=New-Tempdir
		$res=Expand-ArchiveTo -ARCHIVE $FILE -DEST $tmpdir.fullname -OW
		$fonts=ls $(Join-Path $tmpdir.fullname $Item.installfonts)
		$fontsdir=$objFonter.self.path
		foreach($font in $fonts){
			$fontfile=Join-Path $fontsdir $font.name
			if(-not $(Test-Path $fontfile)){
				$objFonter.CopyHere($font.fullname)
			}else{
				$installed=Get-FileHash -Algorithm MD5 $fontfile
				$installing=Get-FileHash -Algorithm MD5 $font.fullname
				if($installed.Hash -ne $installing.Hash ){
					Write-Host "$($font.name) has different file hash. Installing it again."
					Write-Host "Installed: " $installed " To be installed:" $installing
					Remove-Item $fontfile -Force
					$objFonter.CopyHere($font.fullname)
				}else{
					Write-Host "$($font.name) is already installed. (skipped)"
				}
			}

		}
		$tmpdir.delete($true)
	}

  # Single executable target のインストール
  if ($Item.singleExe -ne $null) {
    remove-item $FILE -Stream Zone.Identifier -ErrorAction Ignore
    if (-not (Test-Path $STSingleExeDir)) {
      new-item $STSingleExeDir -ItemType Directory
    }
    Append-Path $STSingleExeDir
    Copy-Item -path $FILE -destination $(Join-Path $STSingleExeDir $Item.singleExe) -Force
  }

  #  Portable App のインストール
  if($Item.extract -ne $null){
    if(Test-Path $item.extract[1]){
          Echo "$($item.name) is already installed.	(Remove	'$($item.extract[1])' to install it	again) "
      }else{
          $res=Expand-ArchiveTo -Archive $FILE -SRC $Item.extract[0] -DEST $Item.extract[1]
      }
      if($Item.startup -ne $null){
          Register-Startup $Item.name $(Join-Path $Item.extract[1] $Item.startup)
      }
  }
}

function Generate-ChocoShim{
	param([String] $Target,
		[String[]] $parents,
		[Switch] $Gui)

#	Write-Host "Generate-ChocoShim -target=$target -parents=$parents"
	if(-not $(Split-Path $Target -IsAbsolute)){
		foreach ($p in $parents){
			$tgt=Join-Path $p $Target
			if(Test-Path $tgt){
				if($gui){
				Generate-ChocoShim -Target $tgt -Gui
				}else{
				Generate-ChocoShim -Target $tgt
				}
			}
		}
		return
	}

	$cbin=$ChocolateyBinPath
	$ctools=$ChocolateyToolsPath
	$shimgen=Join-Path $ctools shimgen.exe
	$shimexe=Join-Path $cbin $(Split-Path -leaf $Target)
	if(-not(Test-Path $shimexe))
	{
		Write-Host "Generating shim $Target"
		if(Test-Path $Target){
			if($Gui){
				& $shimgen -o="$shimexe" -p="$tgt" --gui
			}else{
				& $shimgen -o="$shimexe" -p="$tgt"
			}

		}
	}else{
		Write-Host "A shim executable for [$Target] is already exists"
	}
}

#############################################################################

# append をユーザ環境変数のvarに追加する
# すでに入っていたら何もしない
# var は PATH と同様の ; でセパレートされた値の列とする。
function Add-EnvVar{
	param([string] $Var,
		[string] $Entry,
		[switch] $Prepend,
		[switch] $Machine)
	if($Machine){
		$vartype='Machine'
	}else{
		$vartype='User'
	}
	$v=[Environment]::GetEnvironmentVariable($Var,$vartype)
	if($v.length -eq 0){
		$v=$Entry
	}
	else{
		$vlist=$v.split(';')
		if ($vlist.contains($Entry)){
			return
		}
		if($Prepend){
			if($v -ne ""){
				$v=$Entry+";$v"
			}else{
				$v=$Entry
			}
		}else{
			if ($v -ne "" -and (-not $v.EndsWith(";"))){
				$v+=";"
			}
			$v+=$Entry
		}
	}
	Write-Verbose "Add-EnvVar [$Var]<-[$v]  type=$vartype"
	[Environment]::SetEnvironmentVariable($Var,$v,$vartype)|Out-Null
}

# newpath をユーザ環境変数のPATHに追加する
# すでに入っていたら何もしない
function Append-Path{
	param([string] $newpath)
	Add-EnvVar -var "PATH" -Entry "$newpath"
}

#function Append-Path{
#	param([string] $newpath)
#	$path=[Environment]::GetEnvironmentVariable('PATH','User')
#	$plist=$path.split(';')
#	if (-not($plist.contains($newpath)))
#	{
#		if ($path -ne "" -and (-not $path.EndsWith(";"))){
#			$path+=";"
#		}
#	$path+=$newpath
#	[Environment]::SetEnvironmentVariable('Path',$path,'User')|Out-Null
#	}
#}

# PATH 更新
# https://stackoverflow.com/questions/14381650/how-to-update-windows-powershell-session-environment-variables-from-registry
function Update-Environment {
    $locations = 'HKLM:\SYSTEM\CurrentControlSet\Control\Session Manager\Environment',
                 'HKCU:\Environment'

    $locations | ForEach-Object {
        $k = Get-Item $_
        $k.GetValueNames() | ForEach-Object {
            $name  = $_
            $value = $k.GetValue($_)

            if ($userLocation -and $name -ieq 'PATH') {
                $Env:Path += ";$value"
            } else {
                Set-Item -Path Env:\$name -Value $value
            }
        }

        $userLocation = $true
    }
}


# 新しい一時ディレクトリを作る。
# System.IO.DirectoryInfo を返すので、使い方はこんな感じ
#  $tmpdir=New-TempDir
#  ls $tmpdir.fullname
#  $tmpdir.delete($true)
function New-TempDir{
	$TMP=$(Join-Path $(gc env:tmp) $([system.guid]::newguid().tostring()))
	$folder=New-Item -ItemType Directory -Path $TMP
	#$folder.IsReadOnly=$false
	$folder.Attributes="Normal"
	return $folder
}

# システムのスタートアップに指定した実行ファイルを設定する
function Register-Startup{
param([string] $NAME,   # エントリのの名前
[string] $EXE           # 実行ファイル
)
# https://social.technet.microsoft.com/Forums/en-US/d1c576ca-539f-4ab3-a3cb-9f14851c4eb6/find-all-users-start-menu-startup-folder?forum=winserverpowershell
    $commonStartup = [Environment]::GetFolderPath("CommonStartup")
    $ShortcutPath=$(Join-Path $commonStartup "${NAME}.lnk")
    Write-Host "Target:"+$ShortcutPath
    if( ! $(Test-Path $ShortcutPath))  {
        $objShell	= New-Object -com "Wscript.Shell"
        $objShortcut = $objShell.CreateShortcut($ShortcutPath)
        $objShortcut.TargetPath = $exe
        $objShortcut.Save()
    }
#  $StartupRegPath="HKLM:\Software\Microsoft\Windows\CurrentVersion\Run"
#  $StartList=$(Get-ItemProperty $StartupRegPath)

#  if($StartList.$Name -ne $exe){
#    New-ItemProperty -Path $StartupRegPath -Name $NAME -PropertyType String -Value $EXE -Force
#  }
}


# https://connect.microsoft.com/PowerShell/Feedback/Details/2083343
#  この問題により AtLogonトリガが作れない
# タスクスケジューラにタスクを登録する
function Register-StartupTask{
param([string] $NAME,   # タスクの名前 "\Startups" 以下にこの名前でタスクが作られる
[string] $EXE,          # 実行ファイル
[string] $WDIR=$null,   # 実行時ディレクトリ 指定しなければ実行ファイルのディレクトリ
[string] $USER=$null,         # 実行権限を持つユーザ
[int]    $DELAYSEC=10,  # 起動までの遅延時間  (デフォルトでは10秒)
[switch] $ELEV,         # 管理者権限で実行するかどうかのスイッチ
[switch] $OW,            # 同じ名前のタスクが存在する場合設定を上書きするかどうかのスイッチ 
[switch] $PAUSE         # 実行前にキー入力待ちをするかどうかのスイッチ
)

   $task=Get-ScheduledTask -TaskPath "\Startups\" -TaskName $NAME -ErrorAction Ignore
   if($task -ne $null -and (-not $OW))
   {
     return
   }

 if($WDIR -eq $null -or $WDIR -eq ""){
	$WDIR=Split-Path -Parent $EXE
 }
 if($USER -eq $null -or $USER -eq ""){
   $USER=$env:username
 }

 $args="-Command " + $(Join-path $PSScriptRoot Setup-task.ps1)
 $args=$args+" -EXE `'$EXE`' -WDIR `'$WDIR`' -USER $USER -NAME $NAME -DELAYSEC $DELAYSEC"
 if($OW){
   $args=$args+" -OW"
 }
 if($PAUSE){
	$args=$args+" -Verbose"
 }
 if($ELEV){
  $args=$args+" -ELEV"
 }
 Write-Host "args:$args"
 #Start-Process powershell -ArgumentList $args -wait |Out-Null
 Invoke-Process -Arguments $args 
}

# zip/7z アーカイブを展開する
# ARCHIVE : 書庫ファイル
# DEST : 展開したファイル群を収めるディレクトリ
# SRC : rootに1つだけフォルダがある書庫の場合、その名前を指定すればそれが DESTに展開される
#       rootにファイル群がそのままある書庫の場合指定しなければ root が DEST に展開される
# -OW : DEST が存在してもそのまま展開する
function Expand-ArchiveTo{
  param([string] $ARCHIVE,
        [string] $SRC=".",
        [string] $DEST,
		[switch] $OW)
  $sz=$(Join-Path $(gc Env:\ProgramFiles) "7-zip\7z.exe")
  if(-not(Test-Path $sz)){
	Write-Host "7-zip is not found at $sz. Abort."
	return
  }

  if($OW -eq $true -or -not(Test-Path $DEST)){
    if($src -eq ".") #archiveの中身が直接$DESTに展開されればよい
	{
		Write-Host "  ..extracting files"
		Write-Host "7z x -bb0 -o$($DEST) $ARCHIVE"
        if(-not(Test-Path $DEST)){
           New-Item -type directory -path $DEST
        }
		& $sz x -bb0 -o"$DEST" $ARCHIVE
	}
	else #archiveの中にある $src が $dest になればよい
	{
#      $TMP=$(Join-Path $(gc env:tmp) $([system.guid]::newguid().tostring()))
      $PDEST=Split-Path -Parent $DEST
	  $TMP=$(Join-Path $PDEST $([system.guid]::newguid().tostring()))
      Write-Host "  ..extracting files"
      Write-Host "7z x -bb3 -o$($TMP) $ARCHIVE"
      $args="x -bb0 -o`"$TMP`" `"$ARCHIVE`" `"$SRC`""
	  Start-Process $sz -ArgumentList $args -wait |Out-Null
	  #& $(Join-Path $spath ShowArgs.exe) 7z x -bb3 -o"$TMP" $ARCHIVE
	  $ASRC=$(Join-Path $TMP $SRC)
	  if(-not(Test-Path $ASRC))
	  {
		Write-Host "Unexpected archive structure. Abort."
		return
	  }
#	  $PDEST=Split-Path -Parent $DEST
#      if($PDEST -ne "" -and -not(Test-Path $PDEST)){
#         New-Item -type directory -path $PDEST
#      }
	  do {
		try{
			if($(Get-Item $ASRC).Attrinutes -match "ReadOnly" ){
				Get-Item $ASRC | %{
				$_.Attributes-="ReadOnly"
				}
			}
			if($(Get-Item $ASRC).Attrinutes -match "Hidden" ){
				Get-Item $ASRC | %{
				$_.Attributes-="Hidden"
				}
			}
			if($(Get-Item $ASRC).Attrinutes -match "System" ){
				Get-Item $ASRC | %{
				$_.Attributes-="System"
				}
			}
			Move-Item -Path $ASRC -Destination $dest -ErrorAction Stop
		}catch
		{
			Write-Error "Error moving files. Retrying in 1sec... `n$($error[0])" 
			Start-Sleep 1
		}
		if(Test-Path $ASRC){
			Get-Item $ASRC
		}		
	  }while(Test-Path $ASRC)
	  rm -recurse "$TMP"
	}
  }
}

# 実行ファイルの互換性フラグを設定する
function Set-BinaryCompatibilityFlags
{
	param([string] $EXE,
               [string] $PROP)

	#$layers="HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\layers\"
	$layers="HKCU:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\layers\"
	if(-not(Test-Path $layers)){
		New-Item -ItemType Directory $layers
	}
	$targetlist=Get-Item $layers | %{$_.Property}

	# エントリが存在したらまず削除してから作成する
	if(($targetlist -contains $EXE))
	{
		Remove-ItemProperty -Path $layers -Name $EXE
	}
	New-ItemProperty -Path $layers -Name $EXE -PropertyType String -Value $PROP
}


####################### http://poshcode.org/2767 ちょっとだけ修正
    ## Get-WebFile (aka wget for PowerShell)
    ##############################################################################################################
    ## Downloads a file or page from the web
    ## History:
    ## v3.6 - Add -Passthru switch to output TEXT files
    ## v3.5 - Add -Quiet switch to turn off the progress reports ...
    ## v3.4 - Add progress report for files which don't report size
    ## v3.3 - Add progress report for files which report their size
    ## v3.2 - Use the pure Stream object because StreamWriter is based on TextWriter:
    ##        it was messing up binary files, and making mistakes with extended characters in text
    ## v3.1 - Unwrap the filename when it has quotes around it
    ## v3   - rewritten completely using HttpWebRequest + HttpWebResponse to figure out the file name, if possible
    ## v2   - adds a ton of parsing to make the output pretty
    ##        added measuring the scripts involved in the command, (uses Tokenizer)
    ##############################################################################################################
    function Get-WebFile {
       param(
          $url = (Read-Host "The URL to download"),
          $fileName = $null,
          [string]$referer="",
          [switch]$Passthru,
          [switch]$quiet
       )
       [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
       $req = [System.Net.HttpWebRequest]::Create($url);
       #http://stackoverflow.com/questions/518181/too-many-automatic-redirections-were-attempted-error-message-when-using-a-httpw
       $req.CookieContainer = New-Object System.Net.CookieContainer
#       $req.UserAgent = "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Win64; x64; Trident/6.0)"
#        if($referer.Length -ne 0){
#            $req.Referer = $referer
#        }
#        Write-Host "Header:" $req.Headers
#        Write-Host "Referer:" $req.Referer
#        Write-Host "URI:" $req.RequestUri
        #return $null
       $res = $req.GetResponse();
       if($res -eq $null)
       {return $null}

       if($fileName -and !(Split-Path $fileName)) {
          $fileName = Join-Path (Get-Location -PSProvider "FileSystem") $fileName
       } 
	   elseif((!$Passthru -and ($fileName -eq $null)) -or (($fileName -ne $null) -and (Test-Path -PathType "Container" $fileName)))
       {
          [string]$fileName = ([regex]'(?i)filename=(.*)$').Match( $res.Headers["Content-Disposition"] ).Groups[1].Value
          $fileName = $fileName.trim("\/""'")
          if(!$fileName) {
             $fileName = $res.ResponseUri.Segments[-1]
             $fileName = $fileName.trim("\/")
             if(!$fileName) {
                $fileName = Read-Host "Please provide a file name"
             }
             $fileName = $fileName.trim("\/")
             if(!([IO.FileInfo]$fileName).Extension) {
                $fileName = $fileName + "." + $res.ContentType.Split(";")[0].Split("/")[1]
             }
          }
         $fileName = Join-Path (Get-Location -PSProvider "FileSystem") $fileName
       }
       if($Passthru) {
          $encoding = [System.Text.Encoding]::GetEncoding( $res.CharacterSet )
          [string]$output = ""
       }
     
       if($res.StatusCode -eq 200) {
          [int]$goal = $res.ContentLength
          $reader = $res.GetResponseStream()
          if($fileName) {
             $writer = new-object System.IO.FileStream $fileName, "Create"
          }
          [byte[]]$buffer = new-object byte[] 4096
          [int]$total = [int]$count = 0
          do
          {
             $count = $reader.Read($buffer, 0, $buffer.Length);
             if($fileName) {
                $writer.Write($buffer, 0, $count);
             }
             if($Passthru){
                $output += $encoding.GetString($buffer,0,$count)
             } elseif(!$quiet) {
                $total += $count
                if($goal -gt 0) {
				   if($total -gt $goal){
				   }
                   Write-Progress "Downloading $url" "Saving $total of $goal" -id 0 -percentComplete (($total/$goal)*100)
                } else {
                   Write-Progress "Downloading $url" "Saving $total bytes..." -id 0
                }
             }
          } while ($count -gt 0)
          Write-Progress "Downloading $url" -Completed

          $reader.Close()
          if($fileName) {
             $writer.Flush()
             $writer.Close()
          }
          if($Passthru){
             $output
          }
       }
       $res.Close();
#       if($fileName) {
#          ls $fileName
#       }
       return $fileName
    }
##########################
# http://www.howtogeek.com/tips/how-to-extract-zip-files-using-powershell/
function Expand-ZIPFile($file, $destination)
{
    $shell = new-object -com shell.application
    $zip = $shell.NameSpace($file)
    $zip
    foreach($item in $zip.items())
    {
        $shell.Namespace($destination).copyhere($item)
    }
}

##########################

function Ensure-dir
{
    param([string] $dir)
    if(-not(Test-Path $dir)){
        New-Item -ItemType Directory -Path $dir | Out-Null
        return $dir
    }
    return $null
}

function Check-destdir
{
    param([string] $dir)
	return Ensure-dir $dir
}

if($false){
function Fetch-File
{
	param([string[]]$entry)
	$sha256=$entry[2]
	$fileName=$entry[1]
	$url=$entry[0]
	if($fileName -eq "")
	{$fileName=$null}
    $storedir=Join-path $env:USERPROFILE "Downloads\bulkinstall"
#    if(-not(Test-Path $storedir))
#	{
#		New-Item -ItemType Directory -Path $Downloads
#	}
    $ensure=Check-destdir $storedir
	if($fileName -eq $null)
    {
         $fileName = ($url -replace "%2f","/" -split "/")[-1]
         $fileName = $fileName.trim("\/")
      $fileName = Join-Path -path $storedir -childpath $fileName
   }
    if($fileName -ne $null -and $sha256 -ne $null)
	{
		if(Test-Path $fileName)
		{
			$hash=Get-FileHash $fileName | %{$_.hash}
			if($hash -eq $sha256)
			{
				Write-Host "Cache hit"
				return $fileName
			}
		}
	}
	$curlexe=$(Join-Path ${env:windir} "system32\curl.exe")
	Write-Host $curlexe
	if(test-path $curlexe){
		Write-Host "curl -L $url -o $fileName"
		& $curlexe -L $url -o $fileName
		return $f
	}else{
		Write-Host "Get-WebFile -url $url -filename $fileName"
		$f=Get-WebFile -url $url -filename $fileName
		Write-Host " Got [$f]"
	}
	return $f
}
}

# url      : 取得するURL
# filename : 保存先ファイル名。パスの指定がなければ ~\Downloads\bulkinstall を前置する。省略時はURIから推測。
# check    : チェックサム (アルゴリズムと値の2つを保持する配列) ex: @("SHA256",".........")
function Fetch-File2
{
	param([String] $url,
	[String] $fileName="",
	[String[]] $check,
	[String] $storedir="",
    [String] $referer="",
	[switch] $NOPRESETDEST)
	if($storedir -eq ""){
		$storedir=Join-path $env:USERPROFILE "Downloads\bulkinstall"
	}
    if($NOPRESETDEST){
		$storedir="."
	}
    $ensure=Check-destdir $storedir
	if($fileName -eq "")
    {
        $fileName = ($url -replace "%2f","/" -split "/")[-1]
        $fileName = $fileName.trim("\/")
		$fileName = Join-Path -path $storedir -childpath $fileName
    }
	else
	{
		$parent=Split-Path -Parent $fileName
		if($parent -eq ""){
			$fileName = Join-Path $storedir $fileName
		}

	}
    if($fileName -ne $null -and $check -ne $null)
	{
		if(Test-Path $fileName)
		{
			$hash=Get-FileHash $fileName -Algorithm $check[0] | %{$_.hash}
			if($hash -eq $check[1])
			{
				Write-Host "Cache hit"
				return $fileName
			}
			else
			{
				Write-Host "$hash -ne $($check[1])"
			}
		}
	}
	$curlexe=$(Join-Path ${env:windir} "system32\curl.exe")
	if(test-path $curlexe){
		Write-Host "curl -L $url -o $fileName"
		& $curlexe -L $url -o $fileName
	}
	else{
		Write-Host "Get-WebFile -url $url -filename $fileName -referer $referer"
		$f=Get-WebFile -url $url -filename $fileName -referer $referer
		Write-Host " Got [$f]"
		$fileName=$f
	}
	if(test-path $fileName){
		$hash=Get-FileHash $fileName -Algorithm $check[0] | %{$_.hash}
		if($hash -eq $check[1])
		{
			Write-Host "Hash OK"
			return $fileName
		}
		Write-Host "Unexpected hash:" $hash " , Expected: " $check[1]
	}
	Write-Error "Failed to download $url"
	return ""
}

###############################################################################
# https://github.com/guitarrapc/PowerShellUtil/blob/master/Invoke-Process/Invoke-Process.ps1
# 出力抑制の機能追加
function Invoke-Process
{
	[OutputType([PSCustomObject])]
	[CmdletBinding()]
	param
	(
	[Parameter(Mandatory = $false, Position = 0)]
	[string]$FileName = "PowerShell.exe",
	[Parameter(Mandatory = $false, Position = 1)]
	[string]$Arguments = "",
	[Parameter(Mandatory = $false, Position = 2)]
	[string]$WorkingDirectory = ".",
	[Parameter(Mandatory = $false, Position = 3)]
	[TimeSpan]$Timeout = [System.TimeSpan]::FromMinutes(600),
	[Parameter(Mandatory = $false, Position = 4)]
	[System.Diagnostics.ProcessPriorityClass]$Priority = [System.Diagnostics.ProcessPriorityClass]::Normal,
	[Parameter(Mandatory = $false, Position = 5)]
	[Switch] $Silent,
	[Parameter(Mandatory = $false)]
	[Switch] $runas
	)
	end
	{
		try
		{
			# new Process
			$process = NewProcess -FileName $FileName -Arguments $Arguments -WorkingDirectory $WorkingDirectory -runas:$runas

			# Event Handler for Output
			$stdSb = New-Object -TypeName System.Text.StringBuilder
			$errorSb = New-Object -TypeName System.Text.StringBuilder
			if($Silent)
			{
				$scripBlockOut =
				{
					$x = $Event.SourceEventArgs.Data
					if (-not [String]::IsNullOrEmpty($x))
					{
						$Event.MessageData.AppendLine($x)
					}
				}
				$scripBlockErr =$scripBlockOut
			}
			else
			{
				$scripBlockOut =
				{
					$x = $Event.SourceEventArgs.Data
					if (-not [String]::IsNullOrEmpty($x))
					{
						[System.Console]::WriteLine("[S]"+$x)
						$Event.MessageData.AppendLine($x)
					}
				}
				$scripBlockErr =
				{
					$x = $Event.SourceEventArgs.Data
					if (-not [String]::IsNullOrEmpty($x))
					{
						[System.Console]::WriteLine("[E]"+$x)
						$Event.MessageData.AppendLine($x)
					}
				}
			}
      $stdEvent = Register-ObjectEvent -InputObject $process -EventName OutputDataReceived -Action $scripBlockOut -MessageData $stdSb
      $errorEvent = Register-ObjectEvent -InputObject $process -EventName ErrorDataReceived -Action $scripBlockErr -MessageData $errorSb
			# execution
			$process.Start() | Out-Null
			$process.PriorityClass = $Priority
      $process.BeginOutputReadLine()
      $process.BeginErrorReadLine()
      $process | VerboseOutput
			# wait for complete
			"Waiting for command complete. It will Timeout in {0}ms" -f $Timeout.TotalMilliseconds | VerboseOutput
			$isTimeout = $false
			if (-not $Process.WaitForExit($Timeout.TotalMilliseconds))
			{
			$isTimeout = $true
			"Timeout detected for {0}ms. Kill process immediately" -f $Timeout.TotalMilliseconds | VerboseOutput
			$Process.Kill()
			}
			$Process.WaitForExit()
      $Process.CancelOutputRead()
      $Process.CancelErrorRead()
      # Unregister Event to recieve Asynchronous Event output (You should call before process.Dispose())
      Unregister-Event -SourceIdentifier $stdEvent.Name
      Unregister-Event -SourceIdentifier $errorEvent.Name
      # verbose Event Result
      $stdEvent, $errorEvent | VerboseOutput
			# Get Process result
			return GetCommandResult -Process $process -StandardStringBuilder $stdSb -ErrorStringBuilder $errorSb -IsTimeOut $isTimeout
		}
		finally
		{
			if ($null -ne $process){ $process.Dispose() }
			if ($null -ne $stdEvent){ $stdEvent.StopJob(); $stdEvent.Dispose() }
			if ($null -ne $errorEvent){ $errorEvent.StopJob(); $errorEvent.Dispose() }
		}
		}
		begin
		{
		function NewProcess
		{
			[OutputType([System.Diagnostics.Process])]
			[CmdletBinding()]
			param
			(
				[parameter(Mandatory = $true)]
				[string]$FileName,
				[parameter(Mandatory = $false)]
				[string]$Arguments,
				[parameter(Mandatory = $false)]
				[string]$WorkingDirectory,
				[switch]$runas
			)
			"Command to execute: '{0} {1}', WD: '{2}' runas: {3}" -f $FileName, $Arguments, $WorkingDirectory, $runas | VerboseOutput
			# ProcessStartInfo
			$psi = New-object System.Diagnostics.ProcessStartInfo
			$psi.CreateNoWindow = $true
			$psi.LoadUserProfile = $true
			$psi.UseShellExecute = $false
			$psi.RedirectStandardOutput = $true
			$psi.RedirectStandardError = $true
			$psi.FileName = $FileName
			$psi.Arguments+= $Arguments
			$psi.WorkingDirectory = $WorkingDirectory
      $psi | VerboseOutput
			# Set Process
			$process = New-Object System.Diagnostics.Process
			$process.StartInfo = $psi
			$process.EnableRaisingEvents = $true
			return $process
		}
		function GetCommandResult
		{
			[OutputType([PSCustomObject])]
			[CmdletBinding()]
			param
			(
				[parameter(Mandatory = $true)]
				[System.Diagnostics.Process]$Process,
				[parameter(Mandatory = $true)]
				[System.Text.StringBuilder]$StandardStringBuilder,
				[parameter(Mandatory = $true)]
				[System.Text.StringBuilder]$ErrorStringBuilder,
				[parameter(Mandatory = $true)]
				[Bool]$IsTimeout
			)
			'Get command result string.' | VerboseOutput
			return [PSCustomObject]@{
				StandardOutput = $StandardStringBuilder.ToString().Trim()
				ErrorOutput = $ErrorStringBuilder.ToString().Trim()
				ExitCode = $Process.ExitCode
				IsTimeOut = $IsTimeout
			}
		}
		filter VerboseOutput
		{
			$_ | Out-String -Stream | Write-Verbose
		}
	}
}

# パッチを適用
# Apply-Patch ($patch, $WorkingDirectory, $p, $sysinfo)
# 以下の順で探して見つかった patch.exe を使う
#  $sysinfo["patch.exe"]
#  $sysinfo["gnupackbin"]\patch.exe
#  $sysinfo["gitbin"]\patch.exe
Function Apply-Patch{
	param(
		[parameter(Mandatory=$true, Position=0)]
		[String] $Patch,
		[parameter(Mandatory=$true, Position=1)]
		[String] $WorkingDirectory,
		[parameter(Mandatory=$false, Position=2)]
		[int] $p=0,
		[parameter(Mandatory=$false, Position=3)]
		[hashtable]$sysinfo=@{},
		[parameter(Mandatory=$false)]
		[Switch] $contextDiff,
		[parameter(Mandatory=$false)]
		[String]$OptionalArgs
	)
	$patchexe=Find-Patch $sysinfo
	if($patchexe -eq $null -or $patchexe -eq ""){
		throw  "Patch.exe is not found"
	}
	$arg="-i `"$Patch`" -p $p -f "
	#$arg="-i $Patch -p $p -f "
	if($contextDiff){
		$arg+=" -c"
	}
	else{
		$arg+=" -u"
	}
	if($OptionalArgs -ne ""){
		$arg+=" $OptionalArgs"
	}
	Write-Host "[$patchexe $arg] running in $workingDirectory"
	$res=Invoke-Process -WorkingDirectory $WorkingDirectory -Arguments $arg -FileName $patchexe
	return $res
}

# Version 番号文字列の比較
function Compare-VersionLT{
param(
	[parameter(Mandatory=$true, Position=0)]
	[String]$Version1,
	[parameter(Mandatory=$true, Position=1)]
	[String]$Version2
	)
	if($Version1 -eq $Version2){return $false}
	$v1=$Version1.split(".")
	$v2=$Version2.split(".")
	for($i=0;$i -lt $v1.Count; $i++){
		if([int]$v1[$i] -gt [int]$v2[$i]){
			return $false
		}
	}
	return $true
}

# --------------------------------------------------------------------------------------------------
#  Tool finder
#

Function Find-Patch{
	param(
			[parameter(Mandatory=$false)]
			[hashtable]$sysinfo=@{}
	)
	# patch.exe をシステムから探す
	if($sysinfo.Count -ne 0)
	{
		$checkpaths=@()
		if($sysinfo.contains("patchexe")){$checkpath.Add($sysinfo["patchexe"])}
		if($sysinfo.contains("gnupackbin")){$checkpath.Add($(Join-Path $sysinfo["gnupackbin"] "patch.exe"))}
		if($sysinfo.contains("gitbin")){$checkpath.Add($(Join-Path $sysinfo["gitbin"] "patch.exe"))}
		foreach($pp in $checkpath){
			if(Test-Path $pp){
				$patchexe=$pp
				break
			}
		}
	}
	if($patchexe -eq $null -or $(-not $(Test-Path $patchexe))){
		#sysinfo で見つからなかった or 何もヒントが与えられなかったときに適当に探す
		$patchpaths=@(
		@("C:\Opt\Apps\","gnupack*","app\cygwin\cygwin\bin\patch.exe"),   # Opt\Apps 以下の Gnupack を探す
		@("${env:ProgramFiles}\git\bin","","patch.exe"),      #ProgramFiles\git\bin を探す
		@("${env:ProgramFiles(x86)}\git\bin","","patch.exe"),
		@("C:\Anaconda3\pkgs\","patch-*","patch.exe"), # C:\Anaconda3\pkgs 以下の patch を探す
		@("C:\Anaconda3\Library\bin","","patch.exe")   # C:\Anaconda3\Library\bin を探す
		)
		foreach($pp in $patchpaths){
			if(Test-Path $pp[0])
			{
				gci $pp[0] | ?{$_.Name -like $pp[1]}|%{
					$_patchexe=$(Join-Path $_.FullName $pp[2])
					#Write-Host $_.FullName + $pp[2]
					if(Test-Path $_patchexe){
						$patchexe=$_patchexe
						break
					}
				}
			}
		}
		if(-not $(Test-Path $patchexe))
		{
			return $null
		}
	}
	return $patchexe
}

function Check-CMake{
param(
[parameter(Mandatory=$true, Position=0)]
$Key
)
	$cmpath=$($Key|Get-ItemProperty).'(default)'
	$cmexe=Join-Path $cmpath "bin\cmake.exe"
	if(Test-Path $cmexe){
		return $cmexe
	}
	return ""
}
function Find-CMake{
	$machine_key='HKLM:\SOFTWARE\kitware\'
	$machine_key6432='HKLM:\SOFTWARE\Wow6432Node\kitware\'
	$keys=Get-ChildItem -Path @($machine_key, $machine_key6432) -ErrorAction SilentlyContinue |
	?{ $_.Name -match "CMake"}
	if($keys -eq $null){
		Throw "CMake is not installed"
		return
	}
	$cmexe=""
	if($keys.getType().Name -ne "RegistryKey"){
		$cmkey=$keys[0]
		$v=$cmkey.Name.split(' ')[1]
		foreach($k in $keys)
		{
			$kv=$keys.Name.split(' ')[1]
			$cme=Check-CMake $k
			if($cme -ne "" -and $(Compare-VersionLT $v $kv)){
				$cmexe=$cme
				$v=$kv
			}
		}
	}
	else
	{
		$cmexe=Check-CMake $keys
	}
	if($cmexe -ne ""){
		return $cmexe
	}
	Throw "CMake.exe is not found"
}

function Test-ChocoPkg{
param(
[parameter(Mandatory=$true, Position=0)]
$Pkg
)
	$ChocoLibDir=Join-Path $env:CHocolateyInstall "lib"
	if (Test-Path $(Join-Path $chocoLibDir $Pkg)){
		return $true
	}
	return $false
}

# https://p0w3rsh3ll.wordpress.com/2012/04/18/assoc-and-ftype/
Function Get-Assoc             
{            
[CmdletBinding(DefaultParameterSetName='Find', SupportsTransactions=$false)]            
param(            
   [Parameter(ParameterSetName='Find', ValueFromPipeline=$true, Mandatory=$false, Position=0)]            
    [system.string[]]${Find},            
            
   [Parameter(ParameterSetName='Set', ValueFromPipeline=$false, Mandatory=$true, Position=0)]            
    [System.Management.Automation.SwitchParameter]${Set},            
            
   [Parameter(ParameterSetName='Set', ValueFromPipeline=$false, Mandatory=$true, Position=1)]            
   [ValidatePattern('^\.([a-z0-9]){1,}$')]            
    [system.string]${Extension},            
            
            
   [Parameter(ParameterSetName='Set', ValueFromPipeline=$false, Mandatory=$true, Position=2)]            
    [system.string]${Program}            
            
            
)            
    begin            
    {            
        # Check if we've got the value from the pipeline            
        $direct = $PSBoundParameters.ContainsKey('Find')            
    }            
    process            
    {            
        switch ($PsCmdlet.ParameterSetName)            
        {             
            Find {            
                $resultsar = @()            
                foreach ($item in $Find)            
                {            
                    # Reset variables            
                    $foundassoc = $foundprog = $assoc = $null            
                    # Write-Verbose -Message "Dealing with $item" -Verbose:$true            
                    try            
                    {            
                        $foundassoc = Get-ItemProperty -LiteralPath ("HKLM:\Software\Classes\"  + $item) -ErrorAction Stop            
                    }            
                    catch             
                    {            
                       if ($direct) { Write-Host -ForegroundColor Red -Object "File association not found for extension for $item"}            
                    }            
                    if ($foundassoc -ne $null)            
                    {            
                        if ($foundassoc.Count -ne 0)            
                        {            
                            $assoc = $foundassoc.'(default)'            
                            if ($assoc -ne $null)            
                            {            
                                try            
                                {            
                                    $foundprog = @(Get-ItemProperty -LiteralPath ("HKLM:\Software\Classes\"  + $assoc + "\shell\open\command") -ErrorAction Stop)            
                                }             
                                catch             
                                {            
                                    if($direct){ Write-Host -ForegroundColor Red -Object "File type `'$assoc`' not found or no open command associated with it."}            
                                }            
                                if ($foundprog -ne $null)            
                                {            
                                    $resultsar +=  New-Object -TypeName PSObject -Property @{            
                                        Extension = $item            
                                        Association = $assoc            
                                        Program = $foundprog.'(default)'            
                                    }            
                                } else {            
                                    $resultsar +=  New-Object -TypeName PSObject -Property @{            
                                        Extension = $item            
                                        Association = $assoc            
                                        Program = "NoOpen"            
                                    }            
                                }            
                            } else {            
                                $resultsar +=  New-Object -TypeName PSObject -Property @{            
                                    Extension = $item            
                                    Association = "NoOpen"            
                                    Program = ""            
                                }            
                            }            
                        }            
                    } else {            
                        $resultsar +=  New-Object -TypeName PSObject -Property @{            
                            Extension = $item            
                            Association = "Unknown"            
                            Program = ""            
                        }            
                    }            
                }            
                # Output results            
                return $resultsar            
            } # end of find            
            Set {            
                # Define some common parameters            
                $extraparams = @{}            
                $extraparams += @{Force = $true ; Verbose  = $false ; ErrorAction = 'Stop'}            
                $key = "HKLM:\Software\Classes\" + $Extension            
                $assocfile = ($Extension -replace "\.","") + "file"            
                if (-not(Test-Path -Path $key))            
                {            
                    try            
                    {            
                        New-Item -Path $key @extraparams | Out-Null            
                    }            
                    catch            
                    {            
                        # Unable to achieve above command            
                        switch ($_)            
                        {            
                            {$_.CategoryInfo.Reason -eq 'UnauthorizedAccessException' } { $reason = "access is denied" }            
                            default { $reason  = $_.Exception.Message }            
                        }            
                        Write-Host -ForegroundColor Red -Object "Failed to create key $key because $reason.`nNote that admin rights are required for this operation."            
                    }            
                }            
                if (Test-Path -Path $key)            
                {            
                    try            
                    {            
                        Set-ItemProperty -Path $key -Name '(default)' -Value $assocfile @extraparams            
                    }            
                    catch            
                    {            
                        # Unable to achieve above command            
                        switch ($_)            
                        {            
                            {$_.CategoryInfo.Reason -eq 'UnauthorizedAccessException' } { $reason = "access is denied" }            
                            default { $reason  = $_.Exception.Message }            
                        }            
                        Write-Host -ForegroundColor Red -Object "Failed to set association $Extension because $reason"            
                    }            
                    # If previous operation where we set the value succeeded, continue as we are sure that we have admin rights            
                    if (-not($?))            
                    {            
                        $programkey = "HKLM:\Software\Classes\" + $assocfile + "\shell\open\command"            
                        if (-not(Test-Path -Path $programkey))            
                        {            
                                New-Item -Path $programkey @extraparams | Out-Null            
            
                        }             
                        if (Test-Path -Path $programkey)            
                        {            
                                Set-ItemProperty -Path $programkey -Name '(default)' -Value $program @extraparams            
                        }            
                    }            
                }            
            } # end of set            
        } # end of switch            
    } # end of process            
    end {}            
} # end of function


########################
function TryInstall-PSModule {
    param(
        [parameter(Mandatory=$true, Position=0)]
        [String]$Name
    )
    $pkg=$(Get-InstalledModule | ?{ $_.Name -eq $Name })
    if($pkg -eq $null){
        Write-Host "Installing Module [$Name]"
        Install-Module -Name $Name
    }else{
        Write-Host "$Name is already installed."
    }
}

########################

Export-ModuleMember -Function Read-Confirmation

Export-ModuleMember -Function Test-Elevated
Export-ModuleMember -Function Enter-ElevatedPS
Export-ModuleMember -Function Compare-VersionLT

Export-ModuleMember -Function Expand-ArchiveTo
Export-ModuleMember -Function Expand-ZIPFile

Export-ModuleMember -Function Get-WebFile
#Export-ModuleMember -Function Fetch-File
Export-ModuleMember -Function Fetch-File2
Export-ModuleMember -Function Invoke-Process
Export-ModuleMember -Function Apply-Patch

Export-ModuleMember -Function New-TempDir
Export-ModuleMember -Function Ensure-dir
Export-ModuleMember -Function Check-destdir

Export-ModuleMember -Function Find-Patch
Export-ModuleMember -Function Find-CMake
Export-ModuleMember -Function Test-ChocoPkg

Export-ModuleMember -Function install-targets
Export-ModuleMember -Function install-fontTargets
Export-ModuleMember -Function install-singletarget
Export-ModuleMember -Function Generate-ChocoShim
Export-ModuleMember -Function Set-BinaryCompatibilityFlags

Export-ModuleMember -Function Register-StartupTask
Export-ModuleMember -Function Register-Startup

Export-ModuleMember -Function Add-EnvVar
Export-ModuleMember -Function Append-Path
Export-ModuleMember -Function Update-Environment
Export-ModuleMember -Function TryInstall-PSModule

Export-ModuleMember -Function Get-Assoc
