﻿# !!!  Setup-Psfuncs::Register-StartupTask から で起動されるスクリプト

[CmdletBinding()]
param([string] $NAME,   # タスクの名前 "\Startups" 以下にこの名前でタスクが作られる
[string] $EXE,          # 実行ファイル
[string] $WDIR,         # 実行時ディレクトリ 指定しなければ実行ファイルのディレクトリ
[string] $USER,         # 実行権限を持つユーザ
[int]    $DELAYSEC=10,  # 起動までの遅延時間  (デフォルトでは10秒)
[switch] $ELEV,         # 管理者権限で実行するかどうかのスイッチ
[switch] $OW,            # 同じ名前のタスクが存在する場合設定を上書きするかどうかのスイッチ 
[switch] $mofcomp
)

$args="-Command " + $(Join-path $PSScriptRoot Setup-task.ps1)
$args+=" -Name $Name -EXE $EXE -WDIR $WDIR -USER $USER -DELAYSEC $DELAYSEC"
if($ELEV){
	$args += " -ELEV"
}
if($OW){
	$args += " -OW"
}
if($mofcomp){
	$args += " -mofcomp"
}

$spath=Split-Path -parent $MyInvocation.MyCommand.Path
. (Join-Path $spath "Setup-psfuncs.ps1")
. (Join-Path $spath "Setup-Config.ps1")

$isElev=IS-Elevated

if($ELEV -and $(-not $isElev)){
  Invoke-Process -Arguments $args -runas
  Write-Host "elev and not elevated"
  exit
}

if($mofcomp)
{
	if(Is-Elevated){
		mofcomp c:\Windows\system32\wbem\schedprov.mof
		Write-Host "mofcomp"
	}else{
		#Start-Process powershell -ArgumentList $args -Verb "runas"
		Invoke-Process -Arguments $args -runas
		Write-Host "mofcomp and not elevated"
		exit
	}
	
}


#function Register-StartupTaskImpl{
  Write-Verbose "以下のタスクをタスクスケジューラに登録します"
  Write-Verbose "Name: $Name"
  Write-Verbose "Exe: $Exe"
  Write-Verbose "WDir: $WDIR"
  Write-Verbose "User: $User"
  if($ELEV){
   Write-Verbose "Runas Highest runlevel"
  }
  Write-Verbose "At logon with random delay $DelaySec [s]"
 #Write-Verbose "ここでCtrl-Cを押せば登録をキャンセルできます"
 #if($PSCmdlet.MyInvocation.BoundParameters["Verbose"].IsPresent){pause}

   $task=Get-ScheduledTask -TaskPath "\Startups\" -TaskName $NAME -ErrorAction Ignore
   if($task -ne $null -and (-not $OW))
   {
     return
   }
   if($OW){
	   Write-Host "Task [$name] found. Over writing it."
   }else{
	   Write-Host "No task named $name found. Creating one."
   }
 
 if($WDIR -eq $null -or $WDIR -eq ""){
	$WDIR=Split-Path -Parent $EXE
 }
 if($USER -eq $null -or $USER -eq ""){
   $USER=$env:username
 }
 try{
  if($ELEV -eq $true){
   $principal = New-ScheduledTaskPrincipal -UserId $USER -runlevel Highest
  }
  else{
    $principal = New-ScheduledTaskPrincipal -UserId $USER
  }
  $trigger = New-ScheduledTaskTrigger -AtLogOn -RandomDelay (new-timespan -seconds $DELAYSEC)
  $settings = New-ScheduledTaskSettingsSet -DontStopIfGoingOnBatteries -DontStopOnIdleEnd -DisallowHardTerminate
  $settings.ExecutionTimeLimit="PT0S"

  $Action = New-ScheduledTaskAction -Execute $EXE -WorkingDirectory $WDIR
  Register-ScheduledTask -force -TaskPath \Startups -taskName $NAME -Action $Action -Principal $principal -Trigger $trigger -Settings $settings -ErrorAction Stop
 }catch{
  Write-Host "登録できませんでした。"
  Write-Host "Name: $Name"
  Write-Host "Exe: $Exe"
  Write-Host "WDir: $WDIR"
  Write-Host "User: $User"
  if($ELEV){
   Write-Host "Runas Highest runlevel"
  }
  Write-Host "At logon with random delay $DelaySec [s]"
	if(-not $mofcomp){
		Write-Host "次の workaround を試みます。"
		Write-Host "mofcomp c:\Windows\system32\wbem\schedprov.mof"
		$args +=" -mofcomp"
		Invoke-Process -Arguments $args -runas
		exit
	}
 }
