#
# _10_Local_Gitblit.ps1
#

$portablePackageDefs=@(@{
 name="gitblit";
 archive="http://dl.bintray.com/gitblit/releases/gitblit-1.8.0.zip"
 check=@("MD5","09A7B0F9A9CCBA7F998F510241A7B3E9");
 extract=@("gitblit-1.8.0",$(Join-Path $STAppPrefix gitblit-1.8.0));
 enable = $false
  })

install-targets $portablePackageDefs
