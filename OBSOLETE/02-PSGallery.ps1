#
# _02_PSGallery.ps1
#
# PSGallery のモジュールをインストール

if($false){

# 環境設定
$nuget=$(get-packageprovider | ?{ $_.Name -eq "NuGet"})
if($nuget -eq $null){
    Write-Host "Installing Nuget Package Provider"
    Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force | Out-Null
}
Write-Host "NuGet Package Provier found"

$psgallery=$(Get-PSRepository -Name "PSGallery" | %{$_.InstallationPolicy})
if($psgallery -eq "Untrusted"){
Write-Host "Setting PSGallery repository as Trusted"
Set-PSRepository -name PSGallery -InstallationPolicy Trusted
}

function TryInstall-Module {
    param(
        [parameter(Mandatory=$true, Position=0)]
        [String]$Name
    )
    $pkg=$(Get-InstalledModule | ?{ $_.Name -eq $Name })
    if($pkg -eq $null){
        Write-Host "Installing Module [$Name]"
        Install-Module -Name $Name
    }else{
        Write-Host "$Name is already installed."
    }
}

# Install modules
TryInstall-Module VCVars
TryInstall-Module PSVSEnv

}
