#
# _01_Base_AltdragTweak.ps1
#

#
# 互換性フラグの設定
#
$altdragPath=Join-Path $env:USERPROFILE "AppData\Roaming\AltDrag\AltDrag.exe"

# AltDrag は"高DPI環境ではスケーリングしない"としないとうまく動作しない場合がある
# https://github.com/stefansundin/altdrag/issues/7#issuecomment-50701359

Set-BinaryCompatibilityFlags -EXE $altdragPath -PROP "~ HIGHDPIAWARE"


