#
# Setup_localinstall.ps1
#   >>> 01-Base-Fonts.ps1, 10-Local-gnupack13.06.ps1
$spath=Split-Path -parent $MyInvocation.MyCommand.Path
. (Join-Path $spath Setup-psfuncs.ps1)

# �X�N���v�g�̃C���X�g�[��
$Scripts=$env:USERPROFILE+"\PowerShell"
Check-destdir $Scripts
cp (Join-Path $spath .\Setup-psfuncs.ps1) $Scripts

Append-Path $Scripts
Append-Path $(Resolve-Path $(Join-Path $spath ..\Scripts)).path

# �_�E�����[�h��m��
$Downloads=$env:USERPROFILE+"\Downloads\bulkinstall"
Check-destdir $Downloads

$STAppPrefix="C:\opt\Apps"
$fonttargets = @(
@{
 name="migmix-1m";
 archive="http://osdn.jp/frs/redir.php?m=iij&f=%2Fmix-mplus-ipa%2F63544%2Fmigmix-1m-20150712.zip";
 check=@("MD5","3de659097dc4d6b156040c5c25dc4cf9")
 installfonts="migmix*\*.ttf";
 },
@{
 name="migmix-1p";
 archive="http://osdn.jp/frs/redir.php?m=iij&f=%2Fmix-mplus-ipa%2F63544%2Fmigmix-1p-20150712.zip";
 check=@("MD5","6e68c94b3a9cd8e549d32a5edde8c7a5")
 installfonts="migmix*\*.ttf";
 },
@{
 name="migu-1m";
 archive="http://osdn.jp/frs/redir.php?m=iij&f=%2Fmix-mplus-ipa%2F63545%2Fmigu-1m-20150712.zip";
 check=@("MD5","80046949f85da8177d6f1bb319f744f8")
 installfonts="migu*\*.ttf";
 },
@{
 name="migu-2m";
 archive="http://osdn.jp/frs/redir.php?m=iij&f=%2Fmix-mplus-ipa%2F63545%2Fmigu-2m-20150712.zip";
 check=@("MD5","6dac0f64af30cfb006e27d071cfb9f97")
 installfonts="migu*\*.ttf";
 }
)
$apptargets=@(
 # @{
 # name="keyhac";
 # archive="http://crftwr.github.io/keyhac/download/keyhac_170.zip";
 # check=@("MD5","F842940E7ADE51D6DC10B30F750DA1FC");
 # extract=@("keyhac",$(Join-Path $AppPrefix keyhac_170));
 # },
 @{
 name="keyhac";
 archive="http://crftwr.github.io/keyhac/download/keyhac_172.zip";
 check=@("MD5","655420951DC4713EBE6B11C16D1CEEAE");
 extract=@("keyhac",$(Join-Path $STAppPrefix keyhac_172));
 },
 @{
 name="gnupack";
 archive="http://osdn.jp/frs/redir.php?m=iij&f=%2Fgnupack%2F63996%2Fgnupack_devel-13.06-2015.11.08.exe";
 check=@("MD5","C390D6EF466A1AE321470EF1B5574A12");
 extract=@("gnupack_devel-13.06-2015.11.08",$(Join-Path $STAppPrefix gnupack-13.06));
 }, 
@{
 name="emacs";
 archive="https://github.com/chuntaro/NTEmacs64/raw/master/emacs-24.5-IME-patched.zip";
 check=@("MD5","95DA59C402F5A277A0E8A1D982DCEB5A");
 extract=@("emacs-24.5-IME-patched",$(Join-Path $STAppPrefix emacs-24.5));
 },
 @{
 name="clnch";
 archive="http://crftwr.github.io/clnch/download/clnch_332.zip";
 check=@("MD5","F9444E5C92606F37A44B0C5C8B15E985");
 extract=@("clnch",$(Join-Path $STAppPrefix clnch_332));
 },
 @{
 name="";
 archive="";
 check=@("MD5","");
 extract=@("",$(Join-Path $STAppPrefix XX));
#installfonts="";
 enable=$false;
 }
)


install-targets $fonttargets
install-targets $apptargets


#
#  Startup ����^�X�N�̐ݒ�
#
#$user="yos"

$cl=$($apptargets| where-object name -eq "clnch")
Register-StartupTask -ELEV -NAME "clnch" -EXE $(JOIN-Path $cl.extract[1] clnch.exe) -OW

$kh=$($apptargets| where-object name -eq "keyhac")
Register-StartupTask -ELEV -NAME "keyhac" -EXE $(JOIN-Path $kh.extract[1] keyhac.exe) -OW

$em=$($apptargets| where-object name -eq "emacs")
Set-BinaryCompatibilityFlags -EXE $(Join-Path $em.extract[1] bin\runemacs.exe) -PROP "~ HIGHDPIAWARE"

return
# ���̐�͌Â��X�N���v�g



# �_�E�����[�h����t�@�C���̒�`
# URL
# FILENAME (�ȗ���)
# SHA-256 hash  (Get-Filehash $file �Ōv�Z�\)
# �C���X�g�[����
# http://osdn.jp/projects/gnupack/downloads/63996/gnupack_devel-13.06-2015.11.08.exe/
# mirror  iij, jaist
#$GNUPACK ="http://osdn.jp/frs/redir.php?m=iij&f=%2Fgnupack%2F63996%2Fgnupack_devel-13.06-2015.11.08.exe",
#          "",
#          "a9e3b79ed7ff53b91955edd7f3fd38d5fcd33f4539a4d7a89dae3aa5bfe1c21c",
#          "C:\Opt\Apps\gnupack-13.06"
#$NTEMACS ="https://github.com/chuntaro/NTEmacs64/raw/master/emacs-24.5-IME-patched.zip",
#          "",
#		  "BE01A4E3C36307993D42D15FD5B37052DCBFB739EC852315C4BA4B5B9E9E116A",
#		  "C:\Opt\Apps\emacs-24.5"
#$CLNCH   ="http://crftwr.github.io/clnch/download/clnch_332.zip",
#          "",
#          "1B99D7CC234CB91B3E751824F81C822FE8F71533E7D8B32A0DE4B1539A35BFC4",
#          "C:\Opt\Apps\clnch_332"
#$KEYHAC = "http://crftwr.github.io/keyhac/download/keyhac_170.zip",
#          "",
#		  "E693F94E03324DB0EED24EF6423D92FF70F3CDEBCB9F78918CEA35E0D67D72D2",
#		  "C:\Opt\Apps\keyhac_170"
#$TMP=$(gc env:tmp)
# �_�E�����[�h
# Invoke-WebRequest $GNUPACK_URI -OutFile "$Downloads\$GNUPACK" -UserAgent [Microsoft.PowerShell.Commands.PSUserAgent]::FireFox
#Get-Webfile -Url $GNUPACK_URI #-FileName "$Downloads\$GNUPACK"
Write-Host "...gnupack"
$GNUPACK_FILE=Fetch-File $GNUPACK
Extract-ArchiveTo -ARCHIVE $GNUPACK_FILE -SRC "gnupack_devel-13.06-2015.11.08" -DEST $GNUPACK[3]

Write-Host "...ntemacs"
$NTEMACS_FILE=Fetch-File $NTEMACS
Extract-ArchiveTo -ARCHIVE $NTEMACS_FILE -SRC "emacs-24.5-IME-patched" -DEST $NTEMACS[3]

Write-Host "...clnch"
$CLNCH_FILE=Fetch-File $CLNCH
Extract-ArchiveTo -ARCHIVE $CLNCH_FILE -SRC "clnch" -DEST $CLNCH[3]

Write-Host "...keyhac"
$KEYHAC_FILE=Fetch-File $KEYHAC
Extract-ArchiveTo -ARCHIVE $KEYHAC_FILE -SRC "keyhac" -DEST $KEYHAC[3]

