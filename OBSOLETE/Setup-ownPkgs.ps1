#
# Setup_ownPkgs.ps1
#   >>>> OBSOLETED by 01-Base-ChocolateyLocalRepo.ps1


$spath=Split-Path -parent $MyInvocation.MyCommand.Path
. (Join-Path $spath Setup-psfuncs.ps1)

$packages=@"
ttpage
"@
$pkgheader=@"
<?xml version="1.0" encoding="utf-8"?>
    <packages>
"@
$pkgfooter=@"
    </packages>
"@


# install packages

$pkgconfig=Join-Path $spath "packages.config"
$tmp=new-tempdir
$conf=Join-Path $tmp.fullname packages.config

# コメント、空白、改行を削除
$pkglist=$packages -split "\n" | %{($_ -replace "#.*" ) -replace "\s"}
	
$pkgheader | Set-Content -path $conf
$pkglist |where {$_ -match "[0-9a-z]"} | %{ '<package id="' + ($_ -replace '`n') + '" />' | Add-Content -Path $conf}
$pkgfooter | Add-Content -Path $conf
$pkglist=(($packages -split "\n" | % {$_ -replace "#.*"} )  -join " " ) -replace " +"," "
echo $pkglist
chocolatey install -y $conf -s $(Join-Path $spath ..\..\choco) -f

$tmp.delete($true)
