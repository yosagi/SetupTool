﻿#
# Base system installer
#   >>>> OBSOLETED by 00-Base-Chocolatey.ps1

$packages=@"
# base desktop apps
nomacs
7zip.install
listary
altdrag
keepass.install
firefox
mousewithoutborders

# additional apps
putty.install
sumatrapdf.install
teraterm
sysinternals

# developper apps
cmake.install
git.install
hg
tortoisehg
graphviz
doxygen.install
dependencywalker
wireshark
github
pscx

"@
$pkgheader=@"
<?xml version="1.0" encoding="utf-8"?>
    <packages>
"@
$pkgfooter=@"
    </packages>
"@

# install chocolatey
iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))

# install packages
$spath=Split-Path -parent $MyInvocation.MyCommand.Path

$pkgconfig=Join-Path $spath "packages.config"
if(-not(Test-Path $pkgconfig))
{
	echo "no packages.config found."
	# コメント、空白、改行を削除
	$pkglist=$packages -split "\n" | %{($_ -replace "#.*" ) -replace "\s"}
	
	$pkgheader | Set-Content -path $pkgconfig
	$pkglist |where {$_ -match "[0-9a-z]"} | %{ '<package id="' + ($_ -replace '`n') + '" />' | Add-Content -Path $pkgconfig}
	$pkgfooter | Add-Content -Path $pkgconfig
}
$pkglist=(($packages -split "\n" | % {$_ -replace "#.*"} )  -join " " ) -replace " +"," "
echo $pkglist
chocolatey install -y $pkgconfig

$path=(gc env:path)
$path+=";C:\ProgramData\Chocolatey\Tools"
sc Env:\Path -value $path




# invoke local installer
. (Join-Path $spath "Setup-tweak.ps1")
