#
# Setup_Base.ps1
#
#  Base system installer launcher
$spath=Split-Path -parent $MyInvocation.MyCommand.Path
. (Join-Path $spath "Setup-psfuncs.ps1")
. (Join-Path $spath "Setup-Config.ps1")

Get-ChildItem $(Join-Path $spath "Modules\00-Base-*.ps1") | %{
	Echo "Running $_"
	. $_
}
Get-ChildItem $(Join-Path $spath "Modules\01-Base-*.ps1") | %{
	Echo "Running $_"
	. $_
}
pause