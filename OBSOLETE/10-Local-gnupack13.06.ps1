#
# _10_Local_gnupack13.ps1
#

$portablePackageDefs=@(@{
 name="gnupack";
 archive="http://osdn.jp/frs/redir.php?m=iij&f=%2Fgnupack%2F63996%2Fgnupack_devel-13.06-2015.11.08.exe";
 check=@("MD5","C390D6EF466A1AE321470EF1B5574A12");
 extract=@("gnupack_devel-13.06-2015.11.08",$(Join-Path $STAppPrefix gnupack-13.06));
 })

install-targets $portablePackageDefs
