#
# _10_Local_clnch.ps1
#

$portablePackageDefs=@( @{
 name="clnch";
 archive="http://crftwr.github.io/clnch/download/clnch_333.zip";
 check=@("MD5","F6D813649F50071A4BC3E303C1BEF87D");
 extract=@("clnch",$(Join-Path $STAppPrefix clnch_333));
 startup="clnch.exe"
 enable=$false
 }
)

install-targets $portablePackageDefs
