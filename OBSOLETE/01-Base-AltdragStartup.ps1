﻿#
# _01_Base_AltdragStartup.ps1
#

# タスクスケジューラへの起動タスクの登録
$altdragPath=Join-Path $env:USERPROFILE "AppData\Roaming\AltDrag\AltDrag.exe"
Register-StartupTask -ELEV -NAME "AltDrag" -EXE "$altdragPath" -User $env:username
