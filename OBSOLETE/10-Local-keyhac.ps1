#
# _10_Local_keyhac.ps1
#

$portablePackageDefs=@(@{
 name="keyhac";
 archive="http://crftwr.github.io/keyhac/download/keyhac_176.zip";
 check=@("MD5","713E44986FA4D2FD0A55EBAFB57C82F9");
 extract=@("keyhac",$(Join-Path $STAppPrefix keyhac_176));
 })

install-targets $portablePackageDefs
