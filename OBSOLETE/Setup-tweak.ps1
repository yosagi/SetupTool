#
# Setup_tweak.ps1
#
#   >>>> OBSOLETED by 01-Base-PSCX.ps1, 01-Base-CMake.ps1, 01-Base-7z.ps1, 01-Base-git.ps1
$spath=Split-Path -parent $MyInvocation.MyCommand.Path
. (Join-Path $spath Setup-psfuncs.ps1)

# パッケージでの対応が不完全な Shim executable の生成
$cbin="C:\ProgramData\Chocolatey\bin"
$ctools="C:\ProgramData\Chocolatey\tools"
$shimgen=Join-Path $ctools shimgen.exe
$cmakeguiexe=Join-Path $cbin cmake-gui.exe

if(-not(Test-Path $cmakeguiexe))
{
	Write-Host "Generating shim $cmakguieexe"
	$tgt=Join-Path ${env:ProgramFiles(x86)} "\cmake\bin\cmake-gui.exe"
	if(Test-Path $tgt){
		& $shimgen -o="$cmakeguiexe" -p="$tgt" --gui
	}
	$tgt=Join-Path ${env:ProgramFiles} "\cmake\bin\cmake-gui.exe"
	if(Test-Path $tgt){
		& $shimgen -o="$cmakeguiexe" -p="$tgt" --gui
	}
}
$7zexe=$(Join-path $cbin 7z.exe)
if(-not(Test-Path $7zexe))
{
Write-Host "Generating shim $7zexe"
& $shimgen -o $7zexe  -p="C:\Program Files\7-Zip\7z.exe"
}
$gitexe=$(Join-Path $cbin git.exe)
if(-not(Test-Path $gitexe))
{
Write-Host "Generating shim $gitexe"
& $shimgen -o $gitexe  -p="C:\Program Files\git\bin\git.exe"
}


# 環境変数

if(-not($env:PSModulePath -match "pscx")){
	if(Test-Path $(Join-Path ${Env:ProgramFiles(x86)} "\Powershell Community Extensions" )){
		$pscxpath=$(Join-Path $(Join-Path ${Env:ProgramFiles(x86)} "\Powershell Community Extensions" ) PSCX3)
	}elseif(Test-Path $(Join-Path ${Env:ProgramFiles} "\Powershell Community Extensions" )){
		$pscxpath=$(Join-Path $(Join-Path ${Env:ProgramFiles} "\Powershell Community Extensions" ) PSCX3)
	}else{
		Throw "Could not find Power shell community extention in standard places."
	}
        Write-Host "Adding pscx path[$pscxpath] to PSModulePath"
	[Environment]::SetEnvironmentVariable('PSModulePath', "$pscxpath;${env:PSModulePath}",'Machine')
}
