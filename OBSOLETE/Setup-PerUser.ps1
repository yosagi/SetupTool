#
# Setup_PerUser.ps1
#
#   >>>> OBSOLETED by 01-User-AltdragTweak.ps1, 01-User-Homedirectory.ps1
    
$spath=Split-Path -parent $MyInvocation.MyCommand.Path
. (Join-Path $spath Setup-psfuncs.ps1)

# 環境変数
[Environment]::SetEnvironmentVariable('HOME', $($env:USERPROFILE),'User')

# AltDrag
$altdragPath=Join-Path $env:USERPROFILE "AppData\Roaming\AltDrag\AltDrag.exe"
#
# 互換性フラグの設定
#

# AltDrag は"高DPI環境ではスケーリングしない"としないとうまく動作しない場合がある
# https://github.com/stefansundin/altdrag/issues/7#issuecomment-50701359

Set-BinaryCompatibilityFlags -EXE $altdragPath -PROP "~ HIGHDPIAWARE"


# タスクスケジューラへの起動タスクの登録

Register-StartupTask -ELEV -NAME "AltDrag" -EXE "$altdragPath" -User $env:username