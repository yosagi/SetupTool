# Setup Tool

Windows PCの作業環境をある程度自動的にセットアップするPowershell script。以下のようにローカルにあるスクリプトの実行を許可し、

```
set-executionpolicy remotesigned
```

トップレベルのスクリプトを実行すると

```
./Setup-Module.ps1
```

Modules以下のスクリプトが順に実行され、以下の環境が設定される。2回以上実行してもおかしなことは起きないように書いているはずなので、仮に何か失敗しても再実行すればなんとかなる。

特定の環境設定スクリプトだけを動かす場合にはスクリプト名の一部を指定する。また、-WhatIf や -Confirm も使える。
また、Modules以下のスクリプトのうち、二桁の数値に続き'X'がついているものは -nondefault フラグをつけたときだけ実行される。

例: emacs, spacemacsだけインストール

```
./Setup-Module.ps1 emacs
```

例: すべてのスクリプトを対象にし、それぞれ実行するか問い合わせる

```
./Setup-Module.ps1 -Confirm
```

例: emacs, spacemacsを対象にし、それぞれ実行するか問い合わせる

```
./Setup-Module.ps1 -Confirm emacs
```

例: RealForce for Mac を Windowsで使う時のキー再配置ルールをインストール

```
./Setup-Module.ps1 -nondefault RFMac
```

## Chocolatey本体といくつかのパッケージのインストール (00-Base-Chocolatey)

 1. Chocolatey本体
 1. 以下のパッケージ
    1. 一般
        * nomacs  (image viewer)
        * 7zip    (archiver)
        * listary (file search)
        * altdrag
        * keepass (password manager)
        * firefox
        * mousewithoutborders (software kvm switch)
        * putty (ssh client)
        * sumatrapdf (pdf viewer)
        * teraterm (ssh/telnet/serial terminal)
        * sysinternals (utilities)
        * [keypirinha](http://keypirinha.com) (launcher, Alt+Win+Kで起動)
    1. 開発関連
        * cmake
        * git for windows
        * hg
        * tortoisehg
        * graphviz (doxygenに必要)
        * doxygen
        * dependencywalker
        * wireshark (network monitor)
        * ripgrep (高速grep)
        * sourcetree (git frontend)
        * [jom](https://wiki.qt.io/Jom) (parallel nmake like tool. Qt のビルド用)
        * vswhere (VS locator for VS2017 or later)
        * ActivePerl (perl for windows)
        * Anaconda2 (python for windows)
		* Visual Studio Code

## いくつかのChocolateyパッケージの設定

 1. 7zipの7zexeをコマンドラインから起動できるように (01-Base-7z)
 1. cmakeのcmake.exe,cmake-gui.exeをコマンドラインから起動できるように(01-Base-CMake)
 1. gitのgit.exeをコマンドラインから起動できるように(01-Base-git)
 1. anacondaのconda.exeがあるディレクトリをPATHに追加(01-Base-Anaconda)
 1. altdragをログイン時に自動起動するようにタスクスケジューラに登録(01-Base-AltdragStartup)
 1. keypirinhaをログイン時に自動起動するようにタスクスケジューラに登録(01-Base-Keypirinha)
 1. VS Code に以下の拡張をインストール (20-User-code)
     1. AlanWalk.markdown-toc
     1.  DavidAnson.vscode-markdownlint
     1.  geddski.macros
     1.  hnw.vscode-auto-open-markdown-preview
     1.  IBM.output-colorizer
     1.  lfs.vscode-emacs-friendly
     1.  mitaki28.vscode-clang
     1.  ms-vscode.cpptools
     1.  selbh.keyboard-scroll
     1.  twxs.cmake
     1.  vector-of-bool.cmake-tools
     1.  xaver.clang-format

## [Scoop](https://scoop.sh) 本体と extra bucket (00-Base-Scoop)

 1. [scoop](https://scoop.sh)本体をインストール
 1. [aria2](https://aria2.github.io/index-ja.html)(ダウンローダ)をscoopでインストール
 1. extra bucket を追加

## Capslockキーに右Ctrlにマップするレジストリエントリ追加(01-Base-CtrlRemap)

## RealForce for Mac を Windowsで使う時のキー再配置ルールのレジストリエントリ追加(01X-Base-CtrlRemapRFMac)

デフォルトでは実行されない。以下の再配置ルールをインストールする。
 1. 左Capsに右Ctrlを割り当て
 1. 左Alt(Command)をと左Winに
 1. 左Win(Option)を左Altに
 1. Numlock(Clear)をInsertに
 1. 右アプリケーション(Command)を右Altに
 1. 右Alt(Option)を右Winに

## いくつかのフォントをインストール(01-Base-Fonts)

 1. [Myrica](https://github.com/tomokuni/Myrica/) : Rictyの改変版。
 1. [migmix](http://mix-mplus-ipa.osdn.jp/migmix/) : M+とIPAの合成フォント
 1. [migu](http://mix-mplus-ipa.osdn.jp/migu/) : 改変版M+とIPAの合成フォント
 1. [Ricty Diminished](https://github.com/watiko/fonts-rictydiminished) : [Inconsolata](http://levien.com/type/myfonts/inconsolata.html) と [Circle M+](http://mix-mplus-ipa.osdn.jp/mplus/)の合成フォント
 1. [Source Han Code JP/源ノ角ゴシック Code](https://github.com/adobe-fonts/source-han-code-jp) : Adobeのフリーフォント。日本語とASCIIの幅が1.5:1なのが微妙。
 1. [Cica](https://github.com/miiton/Cica) : Powerlineもいける

## PSGalleryのパッケージ設定といくつかのパッケージのインストール(02-PSGallery)

 1. [VCVarsパッケージ](https://www.powershellgallery.com/packages/VCVars/1.0)
 1. [PSVSEnvパッケージ](https://www.powershellgallery.com/packages/PSVSEnv/1.1.1)

## パッケージ化されていないプログラムのインストール

### [IMEパッチ付きNTEmacs](https://github.com/chuntaro/NTEmacs64) (10-Local-emacs)

C:\Opt\Apps\emacs-xx.x 以下にインストールする。

### [spacemacs](https://github.com/syl20bnr/spacemacs) (30-spacemacs)
spacemacsは~/.emacs.d以下にインストールする。.emacs.dが存在する場合はインストールしない。

また、多少修正したspacemacs用の設定を ~/.spacemacs.d/以下にインストールする。~/.spacemacs.dが存在する場合はインストールしない。

### keyhack 1.76 (10-Local-keyhac)

C:\opt\Apps\keyhack_176以下にインストールする。
これももういらないかもしれない。

### kaoriya vim (10-Local-vim-plugins)

[kaoriya.net](https://www.kaoriya.net/software/vim/) で配布されているvim

### git-bug (10-Local-gitbug)

git repository埋め込み型issue tracker

## 環境変数HOMEを設定 (20-User-Homedirectory)

$USERPROFILEの値を$HOMEに設定する

# 設定

ChocolateyでインストールするパッケージはChocoPackages-DefaultList.listに列挙されている。
変更する場合はこれをChocoPackages.listにコピーして編集する。ChocoPackages.listが存在する場合はChocoPackages-DefaultList.listは読まれない。

Setup-config.ps1 を編集すると以下のディレクトリを変更できる。
 * 作業中にダウンロードするファイルの保存先
 * パッケージ化されていないプログラムのインストール先

Setup-All.ps1はModules以下にあるスクリプトを順次実行するスクリプトなので、不要なスクリプトを削除しておけば実行されない。
